DAXME
=====

This platforme is about providing security assistance for Person and company.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
- Python 3.8
- MySQL 5.7
- Pipenv
- [osgeo4w-setup-x86.exe](http://download.osgeo.org/osgeo4w/osgeo4w-setup-x86.exe)

### Installation Procedure

[See Wiki](https://gitlab.com/d.archai/daxme/wikis/Setup-Your-Dev-Enviroment)


Apps
-------

App | version | summary
--- | --- | ---
[daxme](daxme/) | 00.00.1 | This app have the basic core of the project.
[djangorestframework](djangorestframework/) | 3.10.3 | Django REST framework is a powerful and flexible toolkit for building Web APIs as our project DAXME.
[django-rest-auth](django-rest-auth/) | 0.9.5 | Django-rest-auth provides a set of REST API endpoints for user Authentication and Registration.
[django-allauth](django-allauth/) | 0.38.0 | This app is for addressing authentication, registration, account management as well as 3rd party (social) account authentication..
[rest_framework_swagger](rest_framework_swagger/) | 2.2.0 | This app provides a full documentations for How To use DAXME APIs.
[djangorestframework-simplejwt](djangorestframework-simplejwt/) | ~ | This app provides a JWT authentication method.
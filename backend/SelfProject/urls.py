"""main urls."""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

API_TITLE = 'DAXME API'
SCHEMA_VIEW = get_swagger_view(title=API_TITLE)

urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/v1/", include('daxme.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path("api/v1/rest-auth/", include('rest_auth.urls')),
    path("api/v1/rest-auth/registration/", include('rest_auth.registration.urls')),
    path('api/v1/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/v1/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/v1/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    url(r'^$', SCHEMA_VIEW),


]

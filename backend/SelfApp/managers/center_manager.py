"""Define center managers"""
from django.db import models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point


class CenterManager(models.Manager):
    """Define manager for Center model"""

    def get_nearby_centers(self, longitude=0, latitude=0):
        """
        Get all nearby centers ordered by distance between user coordinates (lat,long).
        :return: Set of centers.
        """
        user_location = Point(float(latitude), float(longitude), srid=4326)
        queryset = self.annotate(distance=Distance('address',
                                                   user_location)).order_by('distance')
        return queryset

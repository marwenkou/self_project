"""Define company managers"""
from django.db import models


class CompanyManager(models.Manager):
    """Define manager for Company model"""
    pass

"""Define profile managers."""
from django.db import models


class ProfileManager(models.Manager):
    """Define manager for Profile model"""
    pass

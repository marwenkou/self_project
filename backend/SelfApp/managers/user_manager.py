"""
Define user manager.
"""
from allauth.account.adapter import DefaultAccountAdapter
from daxme.models import (COMPANY_SUPPLIER, COMPANY_CUSTOMER,
                          STANDARD_CUSTOMER, DONE_STATE,
                          NEW_STATE)
from daxme.models.company_model import Company
from daxme.models.user_model import Profile


class RegisterAdapter(DefaultAccountAdapter):
    """This will represent a custom adapter for user registration."""

    def save_user(self, request, user, form, commit=True):  # pylint: disable=W0613
        """
        Overriding this method to update the corresponding profile of a specific user.
        """
        user = super(RegisterAdapter, self).save_user(request, user, form, commit=True)

        data = form.cleaned_data
        user_type = data.get('type', '')
        is_company = user_type in (COMPANY_SUPPLIER, COMPANY_CUSTOMER)
        profile_data = dict()
        # update user model
        user.first_name = data.get('first_name', False)
        user.last_name = data.get('last_name', False)
        # create company object
        if is_company:
            company_data = dict()
            if data.get('siret', False):
                company_data.update(siret=data.get('siret', False))
            if data.get('company_name', False):
                company_data.update(name=data.get('company_name', False))
            if data.get('address', False):
                company_data.update(address=data.get('address', False))
            company_data.update(manager=user)
            company_obj, comp_created = Company.objects.update_or_create(manager=user, defaults=company_data)
            if comp_created:
                profile_data.update(company=company_obj)
        reg_state = DONE_STATE if user_type in (STANDARD_CUSTOMER,
                                                COMPANY_CUSTOMER) else NEW_STATE
        if data.get('address', False):
            profile_data.update(address=data.get('address', False))
        if data.get('invitation_code', False):
            profile_data.update(invitation_code=data.get('invitation_code', False))
        if data.get('phone_number', False):
            profile_data.update(phone_number=data.get('phone_number', False))
        if data.get('date_of_birth', False):
            profile_data.update(date_of_birth=data.get('date_of_birth', False))
        if data.get('description', False):
            profile_data.update(description=data.get('description', False))
        if data.get('city', False):
            profile_data.update(city=data.get('city', False))
        profile_data.update(type=user_type, registration_state=reg_state)
        Profile.objects.update_or_create(user=user, defaults=profile_data)
        return user

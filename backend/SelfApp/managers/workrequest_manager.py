"""Define WorkRequest managers"""
from django.db import models


class WorkRequestManager(models.Manager):
    """Define manager for WorkRequest model"""
    pass

"""Define Document managers"""
from django.db import models


class DocumentManager(models.Manager):
    """Define manager for Document model"""
    pass

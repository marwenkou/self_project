"""Define Proposal managers"""
from django.db import models


class ProposalManager(models.Manager):
    """Define manager for Proposal model"""
    pass


class ProposalLineManager(models.Manager):
    """Define manager for ProposalLine model"""

    def get_by_proposal(self, pk):
        """
        Retrieve proposal line for given proposal id.
        :param pk: Integer
        :return: List of ProposalLine objects
        """
        requests = self.all().filter(proposal=pk).order_by('-created_at')
        return requests

"""Define appointment managers."""
from django.db import models


class AvailableAppointmentManager(models.Manager):
    """Define manager for AvailableAppointment model"""
    pass


class AppointmentRequestManager(models.Manager):
    """Define manager for AppointmentRequest model"""
    pass

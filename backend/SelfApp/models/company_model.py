"""Define Company models"""
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from daxme.managers.company_manager import CompanyManager


class Company(models.Model):
    """
    Define a company model.
    """
    manager = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    siret = models.CharField(max_length=100)
    address = models.TextField(max_length=100, null=True)

    objects = CompanyManager()

    def __str__(self):
        """
        Override this method to format Company object.'
        """
        return self.name

"""Here goes the definition of proposal models."""
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from daxme.managers.proposal_manager import (ProposalManager, ProposalLineManager)
from daxme.models import (PROPOSAL_STATES, PROP_LINE_STATES,
                          DRAFT_STATE)


class Proposal(models.Model):
    """
    This model represent a proposal for one or multiple service requests within a \
    specific period of time.
    """

    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=500, blank=True, null=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    state = models.CharField(max_length=10, choices=PROPOSAL_STATES, default=DRAFT_STATE)

    objects = ProposalManager()

    def __str__(self):
        """
        Override this method to format proposal object.'
        """
        return f'{self.name} - {self.created_by}'


class ProposalLine(models.Model):
    """
    This model represent a request of service for one specif proposal.
    """

    proposal = models.ForeignKey(Proposal, on_delete=models.CASCADE)
    location = models.CharField(max_length=100)
    agent_number = models.IntegerField(default=1)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    state = models.CharField(max_length=10, choices=PROP_LINE_STATES, default=DRAFT_STATE)

    objects = ProposalLineManager()

    def __str__(self):
        """
        Override this method to format proposal line object.'
        """
        return f'{self.location}: {self.start_date} - {self.end_date}'

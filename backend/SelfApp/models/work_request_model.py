"""Here goes the definition of WorkRequest models."""
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from .proposal_model import ProposalLine
from daxme.managers.workrequest_manager import WorkRequestManager
from daxme.models import WORK_REQ_STATES, SENT_STATE


class WorkRequest(models.Model):
    """
    This model define a work request of one specif proposal.
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    proposal_line = models.ForeignKey(ProposalLine, on_delete=models.CASCADE)
    invited_at = models.DateTimeField(auto_now_add=True, blank=True)
    state = models.CharField(max_length=10, choices=WORK_REQ_STATES, default=SENT_STATE)

    objects = WorkRequestManager()

    def __str__(self):
        """
        Override this method to format proposal line object.'
        """
        return f'{self.user}: {self.proposal_line}'

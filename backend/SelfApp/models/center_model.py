"""Here goes the definition of center models."""
from django.contrib.gis.db import models
from daxme.managers.center_manager import CenterManager


class Center(models.Model):
    """
    This model represent center definition
    """
    name = models.CharField(max_length=50)
    address = models.PointField()

    objects = CenterManager()

    def __str__(self):
        """
        Override this method to format center object.'
        """
        return f'{self.name}'

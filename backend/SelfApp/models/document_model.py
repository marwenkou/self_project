"""Here goes the definition of document models."""
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from daxme.managers.document_manager import DocumentManager
from daxme.models import ATTACHMENT_TYPES, ID_CARD


class Document(models.Model):
    """
    This model represent an attachment which will be uploaded by a specific user.
    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    attachment = models.FileField(upload_to=_('documents/'))
    uploaded_at = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=255, blank=True)
    type = models.CharField(max_length=20, choices=ATTACHMENT_TYPES, default=ID_CARD)

    objects = DocumentManager()

    def __str__(self):
        """
        Override this method to format Document object.'
        """
        return self.type

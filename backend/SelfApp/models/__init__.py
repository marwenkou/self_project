"""Define models module and used variables"""
from django.utils.translation import ugettext_lazy as _

# Common variables

DRAFT_STATE = 'draft'
DONE_STATE = 'done'
WAITING_STATE = 'waiting'

#  Document variables

ID_CARD = 'id_card'
LICENCE = 'licence'
CERTIFICATE = 'certificate'
PICTURE = 'picture'
INSURANCE = 'insurance'
RIB = 'rib'
ATTACHMENT_TYPES = (
    (ID_CARD, _('ID Card')),
    (LICENCE, _('Licence')),
    (CERTIFICATE, _('Certificate')),
    (PICTURE, _('Picture')),
    (INSURANCE, _('Insurance')),
    (RIB, _('RIB Account')),
)

#  Proposal variables

PUBLISH_STATE = 'published'
CANCEL_STATE = 'cancelled'
PROPOSAL_STATES = (
    (DRAFT_STATE, _('Draft')),
    (PUBLISH_STATE, _('Published')),
    (CANCEL_STATE, _('Cancelled')),
    (DONE_STATE, _('Done')),
)
OPEN_STATE = 'open'
CLOSE_STATE = 'closed'

PROP_LINE_STATES = (
    (DRAFT_STATE, _('Draft')),
    (OPEN_STATE, _('Opened')),
    (CLOSE_STATE, _('Closed')),
)

# Appointment Variables


CANCELLED_STATE = 'cancelled'
APP_REQ_STATES = (
    (DRAFT_STATE, _('Draft')),
    (WAITING_STATE, _('Wainting')),
    (DONE_STATE, _('Done')),
    (CANCELLED_STATE, _('Cancelled')),
)

# User Variables

STANDARD_CUSTOMER = 'standardCustomer'
COMPANY_CUSTOMER = 'companyCustomer'
AGENT_SUPPLIER = 'agentSUPPLIER'
COMPANY_SUPPLIER = 'companySUPPLIER'

PROFILE_TYPES = (
    (STANDARD_CUSTOMER, _('Standard Customer')),
    (COMPANY_CUSTOMER, _('Company Customer')),
    (AGENT_SUPPLIER, _('Agent Supplier')),
    (COMPANY_SUPPLIER, _('Company Supplier'))
)

NEW_STATE = 'new'

SIGNUP_STATES = (
    (NEW_STATE, _('New')),
    (WAITING_STATE, _('Waiting')),
    (DONE_STATE, _('Done')),
)

# WorkRequest Variables

SENT_STATE = 'sent'
DECLINED_STATE = 'declined'
ACCEPTED_STATE = 'accepted'
CANCELLED_STATE = 'cancelled'

WORK_REQ_STATES = (
    (SENT_STATE, _('Sent')),
    (DECLINED_STATE, _('Declined')),
    (ACCEPTED_STATE, _('Accepted')),
    (CANCELLED_STATE, _('Cancelled')),
)

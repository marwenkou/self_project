"""Define user models"""
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from .company_model import Company
from .level_model import Level
from phone_verify.models import SMSVerification
from phonenumber_field.modelfields import PhoneNumberField
from daxme.managers.profile_manager import ProfileManager
from daxme.models import (PROFILE_TYPES, SIGNUP_STATES,
                          STANDARD_CUSTOMER, NEW_STATE)
import datetime


class Profile(models.Model):
    """
    Contain the definition of profile model. ex: an agent profile, a security company profile...
    A profile is either an agent, a security company, or a profile for regular user.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    level = models.ForeignKey(Level, null=True, on_delete=models.DO_NOTHING)
    company = models.ForeignKey(Company, on_delete=models.DO_NOTHING, null=True)
    date_of_birth = models.DateField(default=datetime.date.today, null=True)
    address = models.TextField(max_length=100, null=True)
    description = models.TextField(max_length=300, null=True)
    phone_number = PhoneNumberField(null=True)
    city = models.CharField(max_length=100, null=True)
    invitation_code = models.CharField(max_length=100, null=True)
    type = models.CharField(max_length=20, choices=PROFILE_TYPES, default=STANDARD_CUSTOMER)
    registration_state = models.CharField(max_length=20, choices=SIGNUP_STATES, default=NEW_STATE)

    objects = ProfileManager()

    @property
    def phone_is_verified(self):
        """Represent an attribute for profile object."""
        try:
            res = SMSVerification.objects.get(phone_number=self.phone_number)
        except SMSVerification.DoesNotExist:
            res = False
        return res and res.is_verified

    def __str__(self):
        """
        Override this method to format profile object.
        """
        return str(self.user)

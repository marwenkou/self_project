"""Level class definition"""
from django.contrib.gis.db import models
from ..managers.level_manager import LevelManager


class Level(models.Model):
    """
    Define all possible levels for an agent.
    """

    name = models.CharField(max_length=100)

    objects = LevelManager()

    def __str__(self):
        """
        Override this method to format Level object.
        """
        return self.name

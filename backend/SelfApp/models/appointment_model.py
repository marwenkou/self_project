"""Here goes the definition of appointment models."""
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from daxme.managers.appointement_manager import (AppointmentRequestManager,
                                                 AvailableAppointmentManager)
from .center_model import Center
from daxme.models import APP_REQ_STATES, DRAFT_STATE


class AvailableAppointment(models.Model):
    """
    This model represent the available appointment for an agent
    """
    date = models.DateTimeField()
    center = models.ForeignKey(Center, on_delete=models.CASCADE)

    objects = AvailableAppointmentManager()

    def __str__(self):
        """
        Override this method to format an available appointment object.'
        """
        return f'{self.center} - {self.date}'


class AppointmentRequest(models.Model):
    """
    This model represent the Appointment Request for an agent
    """

    appointment = models.ForeignKey(AvailableAppointment, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    state = models.CharField(max_length=4, choices=APP_REQ_STATES, default=DRAFT_STATE)

    objects = AppointmentRequestManager()

    def __str__(self):
        """
        Override this method to format a requested appointment from an agent.'
        """
        return f'{self.user} - {self.appointment}'

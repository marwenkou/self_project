"""DAXME urls definition."""
from django.conf.urls.static import static
from django.conf import settings
from .controllers.user import ProfileViewSet, VerificationViewSet
from .controllers.document import DocumentViewSet
from .controllers.proposal import (ProposalViewSet,
                                   ProposalLineViewSet)
from .controllers.work_request import WorkRequestViewSet
from .controllers.company import CompanyViewSet
from .controllers.center import CenterViewSet
from .controllers.level import LevelViewSet
from .controllers.appointment import (AppointmentRequestViewSet,
                                      AvailableAppointmentViewSet)
from rest_framework import routers

# Routers provide an easy way of automatically determining the URL conf.
ROUTER = routers.DefaultRouter()
ROUTER.register(r'documents', DocumentViewSet)
ROUTER.register(r'proposals', ProposalViewSet)
ROUTER.register(r'proposal-lines', ProposalLineViewSet)
ROUTER.register(r'work-requests', WorkRequestViewSet)
ROUTER.register(r'centers', CenterViewSet)
ROUTER.register(r'available-appointments', AvailableAppointmentViewSet)
ROUTER.register(r'appointment-requests', AppointmentRequestViewSet)
ROUTER.register(r'levels', LevelViewSet)
ROUTER.register(r'profiles', ProfileViewSet)
ROUTER.register(r'companies', CompanyViewSet)
ROUTER.register(r'phone', VerificationViewSet, basename='phone')
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = ROUTER.urls + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

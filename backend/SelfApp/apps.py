"""DAXME apps file."""
from django.apps import AppConfig
from django.db.models.signals import post_migrate


def create_group(name, permissions):
    """
    This method will create a given group and relate it with all given permissions.
    """
    from django.contrib.auth.models import Group  # pylint: disable=C0415

    group = Group.objects.get_or_create(name=name)
    [group.permissions.add(permission) for permission in permissions]  # pylint: disable=W0106


def define_daxme_groups(sender, **kwargs):  # pylint: disable=W0613
    """
    This method will ensure that groups and permissions are created and ready to be used when it's \
    needed.
    """

    customer_perms = []
    create_group('customers', customer_perms)


class DaxmeConfig(AppConfig):
    """Definition of daxmeConfig app."""
    name = 'daxme'

    def ready(self):
        """
        Use post migrate function in order to create groups.
        """
        post_migrate.connect(define_daxme_groups, sender=self)

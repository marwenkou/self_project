"""Here we set all Document controllers."""
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from daxme.serializers.document_serializer import DocumentSerializer
from daxme.models.document_model import Document
from daxme.services.document_service import DocumentService


class DocumentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows document attachments to be viewed or edited.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=DocumentService())
        super(DocumentViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = Document.objects.all().order_by('-uploaded_at')
    serializer_class = DocumentSerializer

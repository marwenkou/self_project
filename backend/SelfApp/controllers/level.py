"""Here we set all Level controllers."""
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from daxme.serializers.level_serializer import LevelSerializer
from daxme.models.level_model import Level
from daxme.services.level_service import LevelService


class LevelViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows levels to be viewed or edited.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=LevelService())
        super(LevelViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = Level.objects.all()
    serializer_class = LevelSerializer

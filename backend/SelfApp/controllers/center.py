"""Here we set all center controllers."""
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from daxme.serializers.center_serializer import CenterSerializer
from daxme.models.center_model import Center
from daxme.services.center_service import CenterService


class CenterViewSet(viewsets.ReadOnlyModelViewSet):
    """
     API endpoint that allows centers to be viewed.
     To retrieve the nearby centers according to user's geo coordinates (Lat, Long)
     @params: ex: &latitude=48.86426305229111&longitude=2.35862731900733
    """
    permission_classes = (IsAuthenticated,)
    queryset = Center.objects.all()
    serializer_class = CenterSerializer

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=CenterService())
        super(CenterViewSet, self).__init__(**kwargs)

    def get_queryset(self):
        """
        Get all nearby centers ordered by distance between user coordinates (lat,long).
        :return: Set of centers.
        """
        long = self.request.query_params.get('longitude', 0)
        lat = self.request.query_params.get('latitude', 0)
        queryset = self.service.get_nearby_centers(long=long, lat=lat)
        return queryset

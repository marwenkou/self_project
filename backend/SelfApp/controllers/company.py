"""Here we set all company controllers."""
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from daxme.serializers.company_serializer import CompanySerializer
from daxme.models.company_model import Company
from daxme.services.company_service import CompanyService


class CompanyViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows Companies to be viewed.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=CompanyService())
        super(CompanyViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

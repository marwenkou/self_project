"""Here we set all Work Request controllers."""
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from daxme.serializers.work_request_serializer import WorkRequestSerializer
from daxme.models.work_request_model import WorkRequest
from rest_framework.permissions import IsAuthenticated
from daxme.services.workrequest_service import WorkRequestService


class WorkRequestViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows requests to be viewed or edited.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=WorkRequestService())
        super(WorkRequestViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = WorkRequest.objects.all().order_by('-invited_at')
    serializer_class = WorkRequestSerializer

    @action(detail=True, methods=['PUT'])
    def accept(self, request, pk=None):
        """
        Update invitation state to accepted.
        :param request:
        :return: Response object
        """
        verdict, msg = self.service.accept(self.get_object())
        if msg:
            raise ValidationError(msg)
        return Response({'status': verdict})

    @action(detail=True, methods=['PUT'])
    def decline(self, request, pk=None):
        """
        Update invitation state to declined.
        :param request:
        :return: Response object
        """
        verdict, msg = self.service.decline(self.get_object())
        if msg:
            raise ValidationError(msg)
        return Response({'status': verdict})

    @action(detail=True, methods=['PUT'])
    def cancel(self, request, pk=None):
        """
        Update invitation state to declined.
        :param request:
        :return: Response object
        """
        verdict, msg = self.service.cancel(self.get_object())
        if msg:
            raise ValidationError(msg)
        return Response({'status': verdict})

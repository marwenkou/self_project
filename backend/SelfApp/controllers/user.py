"""Here we set all profile controllers."""
from rest_framework import viewsets
from daxme.serializers.user_serializer import ProfileSerializer
from daxme.models.user_model import Profile
from phone_verify.api import VerificationViewSet as VerifyViewSET
from phone_verify.base import response
from phone_verify.serializers import PhoneSerializer
from phone_verify.services import send_security_code_and_generate_session_token
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from daxme.services.user_service import ProfileService


class ProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows profile to be viewed or edited.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=ProfileService())
        super(ProfileViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class VerificationViewSet(VerifyViewSET):
    """
    API endpoint for phone verification. Inherited from phone_verify app.
    """
    @action(
        detail=False,
        methods=["POST"],
        permission_classes=[IsAuthenticated],
        serializer_class=PhoneSerializer,
    )
    def register(self, request):
        """
        Send an sms with verification code to the connected user.
        """
        # TODO: Use service implementation  # pylint: disable=W0511
        serializer = PhoneSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone_number = str(serializer.validated_data["phone_number"])
        if hasattr(request.user, 'profile'):
            user_phone_number = request.user.profile.phone_number
        else:
            return response.Ok({"error": "User dont have phone number!"})
        if user_phone_number != phone_number:
            return response.Ok({"error": 'You have entered a wrong phone number!'})
        if not request.user.profile.phone_is_verified:
            session_token = send_security_code_and_generate_session_token(
                str(phone_number)
            )
            return response.Ok({"session_token": session_token})
        return response.Ok({"phone_number": f'Your number {phone_number} is already verified!'})

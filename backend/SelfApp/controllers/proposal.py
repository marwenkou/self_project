"""Here we set all the controllers."""
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from daxme.serializers.proposal_serializer import (ProposalSerializer,
                                                   ProposalLineSerializer)
from daxme.models.proposal_model import (ProposalLine, Proposal)
from daxme.services.proposal_service import (ProposalService, ProposalLineService)


class ProposalViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Proposals to be viewed or edited.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=ProposalService())
        super(ProposalViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = Proposal.objects.all().order_by('-created_at')
    serializer_class = ProposalSerializer

    @action(detail=True, methods=['get'])
    def get_lines(self, request, pk=None):
        """
        Get all requests for a proposal of service.
        :param request:
        :param pk: Integer
        :return: Response object
        """
        requests = ProposalLine.objects.get_by_proposal(pk=pk)
        serializer = ProposalLineSerializer(requests, many=True)
        return Response(serializer.data)

    # @action(detail=True, methods=['put'])
    # def publish(self, request, pk=None):
    #     """
    #     Publish given proposal.
    #     :param request:
    #     :param pk:Integer
    #     :return:
    #     """
    #     proposal = self.get_object()
    #     proposal.publish()
    #     proposal.save()
    #     return Response({'status': 'published'})
    #
    # @action(detail=True, methods=['put'])
    # def cancel(self, request, pk=None):
    #     """
    #     Cancel given proposal.
    #     :param request:
    #     :param pk:Integer
    #     :return:
    #     """
    #     proposal = self.get_object()
    #     proposal.cancel()
    #     proposal.save()
    #     return Response({'status': 'cancelled'})
    #
    # @action(detail=True, methods=['put'])
    # def terminate(self, request, pk=None):
    #     """
    #     Finish given proposal.
    #     :param request:
    #     :param pk:Integer
    #     :return:
    #     """
    #     proposal = self.get_object()
    #     proposal.terminate()
    #     proposal.save()
    #     return Response({'status': 'Finished'})


class ProposalLineViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows requests to be viewed or edited.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=ProposalLineService())
        super(ProposalLineViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = ProposalLine.objects.all().order_by('-created_at')
    serializer_class = ProposalLineSerializer

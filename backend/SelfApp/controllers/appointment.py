"""Here we set all appointment controllers."""
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from daxme.serializers.appointment_serializer import (AppointmentRequestSerializer,
                                                      AvailableAppointmentSerializer)
from daxme.models.appointment_model import (AppointmentRequest,
                                            AvailableAppointment)
from daxme.services.appointment_service import (AppointmentRequestService,
                                                AvailableAppointmentService)


class AvailableAppointmentViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows available Appointments to be viewed or edited.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=AvailableAppointmentService())
        super(AvailableAppointmentViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = AvailableAppointment.objects.all()
    serializer_class = AvailableAppointmentSerializer


class AppointmentRequestViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows appointment requests to be viewed or edited for current user.
    """

    def __init__(self, **kwargs):
        """Override this method to add service field to the controller."""
        kwargs.update(service=AppointmentRequestService())
        super(AppointmentRequestViewSet, self).__init__(**kwargs)

    permission_classes = (IsAuthenticated,)
    queryset = AppointmentRequest.objects.all()
    serializer_class = AppointmentRequestSerializer

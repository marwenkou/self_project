"""Here we can manage admin side of DAXME."""
from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin
from .models.center_model import Center
from .models.appointment_model import (AvailableAppointment,
                                       AppointmentRequest)
from .models.level_model import Level


@admin.register(Center)
class CenterAdmin(OSMGeoAdmin):
    """Inherit from OSMGeiAdmin to show geo map in administration panel."""
    list_display = ('name', 'address')


admin.site.register(AvailableAppointment)
admin.site.register(AppointmentRequest)
admin.site.register(Level)

"""Here we have center serializer definitions."""

from rest_framework import serializers
from daxme.models.center_model import Center


class CenterSerializer(serializers.ModelSerializer):
    """Serializer class for Center model."""
    class Meta:  # pylint: disable=C0115
        model = Center
        fields = '__all__'

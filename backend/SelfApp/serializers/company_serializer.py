"""Here we have company serializer definitions."""
from rest_framework import serializers
from daxme.models.company_model import Company


class CompanySerializer(serializers.ModelSerializer):
    """Serializer class for company model."""
    class Meta:  # pylint: disable=C0115
        model = Company
        fields = '__all__'

"""Here we have document serializer definitions."""
from rest_framework import serializers
from daxme.models.document_model import Document


class DocumentSerializer(serializers.ModelSerializer):
    """Serializer class for document model."""
    class Meta:  # pylint: disable=C0115
        model = Document
        fields = ['user', 'attachment', 'type', 'uploaded_at', 'description']

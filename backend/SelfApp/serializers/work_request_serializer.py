"""Here we have WorkRequest serializer definitions."""

from rest_framework import serializers
from ..models.work_request_model import WorkRequest


class WorkRequestSerializer(serializers.ModelSerializer):
    """Serializer class for WorkRequest model."""
    class Meta:  # pylint: disable=C0115
        model = WorkRequest
        fields = '__all__'

"""Here we have proposal serializer definitions."""

from rest_framework import serializers
from daxme.models.proposal_model import Proposal, ProposalLine


class ProposalSerializer(serializers.ModelSerializer):
    """Serializer class for proposal model."""
    class Meta:  # pylint: disable=C0115
        model = Proposal
        fields = ['name', 'description', 'created_by', 'created_at', 'start_date', 'end_date', 'state']


class ProposalLineSerializer(serializers.ModelSerializer):
    """Serializer class for request model."""
    class Meta:  # pylint: disable=C0115
        model = ProposalLine
        fields = ['proposal', 'location', 'created_at', 'start_date', 'end_date', 'agent_number', 'state']

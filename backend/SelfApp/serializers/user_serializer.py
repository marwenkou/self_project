"""Here we have user serializer definitions."""
from rest_auth.serializers import UserDetailsSerializer
from daxme.models.user_model import Profile
from daxme.models import (PROFILE_TYPES,
                          COMPANY_CUSTOMER, COMPANY_SUPPLIER,
                          AGENT_SUPPLIER, STANDARD_CUSTOMER)
from rest_auth.registration.serializers import RegisterSerializer as RootRegSerializer
from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from phonenumber_field import phonenumber
import datetime
import itertools


class ProfileSerializer(serializers.ModelSerializer):
    """
    Serializer class for user profile model.
    """
    phone_is_verified = serializers.ReadOnlyField()

    class Meta:  # pylint: disable=C0115
        model = Profile
        fields = ('date_of_birth', 'phone_number', 'phone_is_verified',
                  'address', 'description', 'city',
                  'invitation_code', 'level', 'company')


class UserSerializer(UserDetailsSerializer):
    """
    Add profile attribute in User detail serializer
    """

    profile = ProfileSerializer()

    def update(self, instance, validated_data):
        """Update both user and profile models."""
        validated_profile = validated_data.pop('profile')
        instance = super(UserSerializer, self).update(instance, validated_data)
        if hasattr(instance, 'profile'):
            for field, value in validated_profile.items():
                setattr(instance.profile, field, value)
            instance.profile.save()
        return instance

    class Meta(UserDetailsSerializer.Meta):  # pylint: disable=C0115
        fields = UserDetailsSerializer.Meta.fields + ('profile',)
        read_only_fields = ('',)


class RegisterSerializer(RootRegSerializer):  # pylint: disable=W0223
    """
    Gives custom serializer for user registration.
    """
    first_name = serializers.CharField(write_only=True)
    last_name = serializers.CharField(write_only=True)
    invitation_code = serializers.CharField(write_only=True, allow_null=True, required=False)
    phone_number = serializers.CharField(write_only=True, required=True)
    address = serializers.CharField(max_length=100, write_only=True, required=False)
    date_of_birth = serializers.DateField(default=datetime.date.today, required=False)
    description = serializers.CharField(max_length=300, allow_null=True, required=False)
    type = serializers.CharField(required=True)
    city = serializers.CharField(max_length=100, required=False)
    # company data
    company_name = serializers.CharField(write_only=True, allow_null=True, required=False)
    siret = serializers.CharField(write_only=True, allow_null=True, required=False)
    company_address = serializers.CharField(write_only=True, allow_null=True, required=False)

    def validate_phone_number(self, phone_number):
        """
        Validate phone_number field
        :param phone_number: Str
        :return: Str
        """
        phone_number = phonenumber.to_python(phone_number)
        if phone_number and not phone_number.is_valid():
            raise serializers.ValidationError(_("Invalid phone number!"))
        return phone_number

    def validate_type(self, type):
        """
        Validate type field
        :param type: Str
        :return: Str
        """
        if type not in itertools.chain(*PROFILE_TYPES):
            raise serializers.ValidationError(_('Invalid type'))
        return type

    def validate_company_name(self, company_name):
        """
        Validate company_name field
        :param company_name: Str
        :return: Str
        """
        type_val = self.initial_data.get('type', False)
        if type_val in (COMPANY_SUPPLIER, COMPANY_CUSTOMER) and not company_name:
            raise serializers.ValidationError("This field may not be blank.")
        return company_name

    def validate_siret(self, siret):
        """
        Validate siret field
        :param siret: Str
        :return: Str
        """
        type_val = self.initial_data.get('type', False)
        if type_val in (COMPANY_SUPPLIER, COMPANY_CUSTOMER) and not siret:
            raise serializers.ValidationError("This field may not be blank.")
        return siret

    def validate_company_address(self, company_address):
        """
        Validate company_address field
        :param company_address: Str
        :return: Str
        """
        type_val = self.initial_data.get('type', False)
        if type_val in (COMPANY_SUPPLIER, COMPANY_CUSTOMER) and not company_address:
            raise serializers.ValidationError("This field may not be blank.")
        return company_address

    def validate_address(self, address):
        """
        Validate address field
        :param address: Str
        :return: Str
        """
        type_val = self.initial_data.get('type', False)
        if type_val in (AGENT_SUPPLIER, STANDARD_CUSTOMER) and not address:
            raise serializers.ValidationError("This field may not be blank.")
        return address

    def get_cleaned_data(self):
        """Return cleaned datas."""
        res = super(RegisterSerializer, self).get_cleaned_data()
        res.update(phone_number=self.validated_data.get('phone_number', ''),
                   address=self.validated_data.get('address', ''),
                   date_of_birth=self.validated_data.get('date_of_birth', ''),
                   description=self.validated_data.get('description', ''),
                   type=self.validated_data.get('type', ''),
                   city=self.validated_data.get('city', ''),
                   first_name=self.validated_data.get('first_name', ''),
                   last_name=self.validated_data.get('last_name', ''),
                   invitation_code=self.validated_data.get('invitation_code', ''),
                   company_name=self.validated_data.get('company_name', ''),
                   company_address=self.validated_data.get('company_address', ''),
                   siret=self.validated_data.get('siret', ''),
                   )
        return res

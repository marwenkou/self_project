"""Here we have appointment serializer definitions."""
from rest_framework import serializers
from daxme.models.appointment_model import (AppointmentRequest,
                                            AvailableAppointment)


class AvailableAppointmentSerializer(serializers.ModelSerializer):
    """Serializer class for available appointment model."""
    class Meta:  # pylint: disable=C0115
        model = AvailableAppointment
        fields = ['center', 'date']


class AppointmentRequestSerializer(serializers.ModelSerializer):
    """Serializer class for appointment request model."""
    class Meta:  # pylint: disable=C0115
        model = AppointmentRequest
        fields = ['appointment', 'user', 'state']

"""Here we have level serializer definitions."""
from rest_framework import serializers
from daxme.models.level_model import Level


class LevelSerializer(serializers.ModelSerializer):
    """
    Serializer class for level model.
    """
    class Meta:  # pylint: disable=C0115
        model = Level
        fields = '__all__'

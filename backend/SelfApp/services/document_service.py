"""Document service definition"""
from daxme.services.interfaces.document_interface import DocumentInterface


class DocumentService(DocumentInterface):
    """
    Contain services for document entity.
    """
    pass

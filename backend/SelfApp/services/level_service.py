"""Level service definition"""
from daxme.services.interfaces.level_interface import LevelInterface


class LevelService(LevelInterface):
    """
    Contain services for Level entity.
    """
    pass

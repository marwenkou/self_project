"""Center service definition"""
from daxme.services.interfaces.center_interface import CenterInterface


class CenterService(CenterInterface):
    """
    Contain services for center entity.
    """
    def get_nearby_centers(self, long=0, lat=0):
        objects = self.manager.get_nearby_centers(longitude=long, latitude=lat)
        return objects

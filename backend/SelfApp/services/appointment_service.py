"""Appointment service"""
from daxme.services.interfaces.appointment_interface import (AppointmentRequestInterface,
                                                             AvailableAppointmentInterface)


class AvailableAppointmentService(AvailableAppointmentInterface):
    """
    Contain services for AvailableAppointment entity.
    """
    pass


class AppointmentRequestService(AppointmentRequestInterface):
    """
    Contain services for AppointmentRequest entity.
    """
    pass

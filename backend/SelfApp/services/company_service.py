"""Company service definition"""
from daxme.services.interfaces.company_interface import CompanyInterface


class CompanyService(CompanyInterface):
    """
    Contain services for company entity.
    """
    pass

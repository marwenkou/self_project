"""User service definition"""
from daxme.services.interfaces.user_interface import (UserInterface, ProfileInterface)


class UserService(UserInterface):
    """
    Contain services for user entity.
    """
    pass


class ProfileService(ProfileInterface):
    """
    Contain services for profile entity.
    """
    pass

"""Appointment interface"""
from daxme.models.appointment_model import (AvailableAppointment, AppointmentRequest)


class AvailableAppointmentInterface:
    """AvailableAppointment interface definition"""
    def __init__(self):
        self.model = AvailableAppointment
        self.manager = self.model.objects


class AppointmentRequestInterface:
    """AppointmentRequest interface definition"""
    def __init__(self):
        self.model = AppointmentRequest
        self.manager = self.model.objects

"""Level interface"""
from daxme.models.level_model import Level


class LevelInterface:
    """Level interface definition"""
    def __init__(self):
        self.model = Level
        self.manager = self.model.objects

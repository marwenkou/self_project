"""Proposal interface"""
from daxme.models.proposal_model import (Proposal, ProposalLine)


class ProposalInterface:
    """Proposal interface definition"""
    def __init__(self):
        self.model = Proposal
        self.manager = self.model.objects


class ProposalLineInterface:
    """ProposalLine interface definition"""
    def __init__(self):
        self.model = ProposalLine
        self.manager = self.model.objects

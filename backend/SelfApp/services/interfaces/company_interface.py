"""Company interface"""
from daxme.models.company_model import Company


class CompanyInterface:
    """Company interface definition"""
    def __init__(self):
        self.model = Company
        self.manager = self.model.objects

"""User interface"""
from django.contrib.auth.models import User
from daxme.models.user_model import Profile


class UserInterface:
    """User interface definition"""
    def __init__(self):
        self.model = User
        self.manager = self.model.objects


class ProfileInterface:
    """Profile interface definition"""
    def __init__(self):
        self.model = Profile
        self.manager = self.model.objects

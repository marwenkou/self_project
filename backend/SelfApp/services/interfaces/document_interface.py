"""Document interface"""
from daxme.models.document_model import Document


class DocumentInterface:
    """Document interface definition"""
    def __init__(self):
        self.model = Document
        self.manager = self.model.objects

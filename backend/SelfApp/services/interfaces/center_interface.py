from daxme.models.center_model import Center


class CenterInterface:
    """Center interface definition"""
    def __init__(self):
        self.model = Center
        self.manager = self.model.objects

    def get_nearby_centers(self, long=0, lat=0):
        """
        Allow to retrieve the nearby centers according to given coordinates.
        :param long: Float
        :param lat: Float
        :return: List of center objects
        """
        return []

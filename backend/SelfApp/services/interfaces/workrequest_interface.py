"""WorkRequest interface"""
from daxme.models.work_request_model import WorkRequest


class WorkRequestInterface:
    """WorkRequest interface definition"""
    def __init__(self):
        self.model = WorkRequest
        self.manager = self.model.objects

    def accept(self, obj):
        """
        An agent can accept a work invitation. Change state to accepted.
        :param obj: Instance of WorkRequest
        :return: tuple (bool, msg)
        """
        return False, ""

    def decline(self, obj):
        """
        An agent can decline a work invitation. Change state to declined.
        :param obj: Instance of WorkRequest
        :return: tuple (bool, msg)
        """
        return False, ""

    def cancel(self, obj):
        """
        An agent can cancel a work invitation. Change state to canbcelled.
        :param obj: Instance of WorkRequest
        :return: tuple (bool, msg)
        """
        return False, ""

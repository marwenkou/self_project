"""proposal service definition"""
from daxme.services.interfaces.proposal_interface import (ProposalInterface, ProposalLineInterface)


class ProposalService(ProposalInterface):
    """
    Contain services for Proposal entity.
    """
    pass


class ProposalLineService(ProposalLineInterface):
    """
    Contain services for ProposalLine entity.
    """
    pass

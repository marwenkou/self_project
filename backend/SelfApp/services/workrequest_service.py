"""WorkRequest service definition"""
from django.utils.translation import ugettext_lazy as _
from daxme.services.interfaces.workrequest_interface import WorkRequestInterface
from daxme.models import (ACCEPTED_STATE, DECLINED_STATE,
                          CANCELLED_STATE, SENT_STATE)


class WorkRequestService(WorkRequestInterface):
    """
    Contain services for WorkRequest entity.
    """
    def accept(self, obj):
        msg = ''
        if obj.state == ACCEPTED_STATE:
            msg = _("Invitation already accepted")
        if obj.state == DECLINED_STATE:
            msg = _("Invitation already declined")
        if obj.state == CANCELLED_STATE:
            msg = _("Invitation already cancelled")
        if obj.state == SENT_STATE:
            obj.state = ACCEPTED_STATE
            obj.save()
            return True, msg
        return False, msg

    def decline(self, obj):
        msg = ''
        if obj.state == ACCEPTED_STATE:
            msg = _("Invitation already accepted")
        if obj.state == DECLINED_STATE:
            msg = _("Invitation already declined")
        if obj.state == CANCELLED_STATE:
            msg = _("Invitation already cancelled")
        if obj.state == SENT_STATE:
            obj.state = DECLINED_STATE
            obj.save()
            return True, msg
        return False, msg

    def cancel(self, obj):
        msg = ''
        if obj.state == CANCELLED_STATE:
            msg = _("Invitation already cancelled")
        if obj.state == DECLINED_STATE:
            msg = _("Invitation already declined")
        if obj.state == SENT_STATE:
            msg = _("You can either accept or decline this work invitation")
        if obj.state == ACCEPTED_STATE:
            obj.state = CANCELLED_STATE
            obj.save()
            return True, msg
        return False, msg
